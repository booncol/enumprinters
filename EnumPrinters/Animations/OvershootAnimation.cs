﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace WpfCustomAnimation
{
    class OvershootAnimation : DoubleAnimation
    {
        private double tension = 2.0;

        public OvershootAnimation()
            : base()
        {
        }

        public OvershootAnimation(double toValue, Duration duration)
            : base(toValue, duration)
        {
        }
 
        public OvershootAnimation(double toValue, Duration duration, FillBehavior fillBehavior)
            : base(toValue, duration, fillBehavior)
        {
        }
 
        public OvershootAnimation(double fromValue, double toValue, Duration duration)
            : base(fromValue, toValue, duration)
        {
        }
  
        public OvershootAnimation(double fromValue, double toValue, Duration duration, FillBehavior fillBehavior)
            : base(fromValue, toValue, duration, fillBehavior)
        {
        }

        protected override Freezable CreateInstanceCore()
        {
            return new OvershootAnimation();
        }

        protected override double GetCurrentValueCore(double defaultOriginValue, double defaultDestinationValue, AnimationClock animationClock)
        {
            double i = (double)animationClock.CurrentProgress - 1.0;
            double t = i * i * ((tension + 1) * i + tension) + 1.0;
            return (double)From + ((double)To - (double)From) * t;
        }
    }
}
