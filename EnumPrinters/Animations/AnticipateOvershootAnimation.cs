﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace WpfCustomAnimation
{
    class AnticipateOvershootAnimation : DoubleAnimation
    {
        private double tension = 3.0;

        public AnticipateOvershootAnimation()
            : base()
        {
        }

        public AnticipateOvershootAnimation(double toValue, Duration duration)
            : base(toValue, duration)
        {
        }

        public AnticipateOvershootAnimation(double toValue, Duration duration, FillBehavior fillBehavior)
            : base(toValue, duration, fillBehavior)
        {
        }

        public AnticipateOvershootAnimation(double fromValue, double toValue, Duration duration)
            : base(fromValue, toValue, duration)
        {
        }

        public AnticipateOvershootAnimation(double fromValue, double toValue, Duration duration, FillBehavior fillBehavior)
            : base(fromValue, toValue, duration, fillBehavior)
        {
        }

        protected override Freezable CreateInstanceCore()
        {
            return new AnticipateOvershootAnimation();
        }

        protected override double GetCurrentValueCore(double defaultOriginValue, double defaultDestinationValue, AnimationClock animationClock)
        {
            double i = (double)animationClock.CurrentProgress;
            double t;

            if (i < 0.5)
            {
                t = 0.5 * __a(i * 2.0, tension);
            }
            else
            {
                t = 0.5 * (__o(i * 2.0 - 2.0, tension) + 2.0);
            }

            return (double)From + ((double)To - (double)From) * t;
        }

        protected double __a(double t, double s)
        {
            return t * t * ((s + 1.0) * t - s);
        }

        protected double __o(double t, double s)
        {
            return t * t * ((s + 1.0) * t + s);
        }
    }
}
