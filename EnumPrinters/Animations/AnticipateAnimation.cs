﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace WpfCustomAnimation
{
    class AnticipateAnimation : DoubleAnimation
    {
        private double tension = 2.0;

        public AnticipateAnimation()
            : base()
        {
        }

        public AnticipateAnimation(double toValue, Duration duration)
            : base(toValue, duration)
        {
        }

        public AnticipateAnimation(double toValue, Duration duration, FillBehavior fillBehavior)
            : base(toValue, duration, fillBehavior)
        {
        }

        public AnticipateAnimation(double fromValue, double toValue, Duration duration)
            : base(fromValue, toValue, duration)
        {
        }

        public AnticipateAnimation(double fromValue, double toValue, Duration duration, FillBehavior fillBehavior)
            : base(fromValue, toValue, duration, fillBehavior)
        {
        }

        protected override Freezable CreateInstanceCore()
        {
            return new AnticipateAnimation();
        }

        protected override double GetCurrentValueCore(double defaultOriginValue, double defaultDestinationValue, AnimationClock animationClock)
        {
            double i = (double)animationClock.CurrentProgress;
            double t = i * i * ((tension + 1.0) * i - tension);
            return (double)From + ((double)To - (double)From) * t;
        }
    }
}
