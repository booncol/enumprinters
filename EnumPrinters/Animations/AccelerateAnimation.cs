﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace WpfCustomAnimation
{
    class AccelerateAnimation : DoubleAnimation
    {
        public AccelerateAnimation()
            : base()
        {
        }

        public AccelerateAnimation(double toValue, Duration duration)
            : base(toValue, duration)
        {
        }

        public AccelerateAnimation(double toValue, Duration duration, FillBehavior fillBehavior)
            : base(toValue, duration, fillBehavior)
        {
        }

        public AccelerateAnimation(double fromValue, double toValue, Duration duration)
            : base(fromValue, toValue, duration)
        {
        }

        public AccelerateAnimation(double fromValue, double toValue, Duration duration, FillBehavior fillBehavior)
            : base(fromValue, toValue, duration, fillBehavior)
        {
        }

        protected override Freezable CreateInstanceCore()
        {
            return new AccelerateAnimation();
        }

        protected override double GetCurrentValueCore(double defaultOriginValue, double defaultDestinationValue, AnimationClock animationClock)
        {
            double t = Math.Pow((double)animationClock.CurrentProgress, 2.0);
            return (double)From + ((double)To - (double)From) * t;
        }
    }
}
