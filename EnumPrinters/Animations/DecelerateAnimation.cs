﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace WpfCustomAnimation
{
    class DecelerateAnimation : DoubleAnimation
    {
        public DecelerateAnimation()
            : base()
        {
        }

        public DecelerateAnimation(double toValue, Duration duration)
            : base(toValue, duration)
        {
        }
 
        public DecelerateAnimation(double toValue, Duration duration, FillBehavior fillBehavior)
            : base(toValue, duration, fillBehavior)
        {
        }
 
        public DecelerateAnimation(double fromValue, double toValue, Duration duration)
            : base(fromValue, toValue, duration)
        {
        }
  
        public DecelerateAnimation(double fromValue, double toValue, Duration duration, FillBehavior fillBehavior)
            : base(fromValue, toValue, duration, fillBehavior)
        {
        }

        protected override Freezable CreateInstanceCore()
        {
            return new DecelerateAnimation();
        }

        protected override double GetCurrentValueCore(double defaultOriginValue, double defaultDestinationValue, AnimationClock animationClock)
        {
            double t = 1.0 - Math.Pow((1.0 - (double)animationClock.CurrentProgress), 2.0);
            return (double)From + ((double)To - (double)From) * t;
        }
    }
}
