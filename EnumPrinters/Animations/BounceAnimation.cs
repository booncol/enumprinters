﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace WpfCustomAnimation
{
    class BounceAnimation : DoubleAnimation
    {
        public BounceAnimation()
            : base()
        {
        }

        public BounceAnimation(double toValue, Duration duration)
            : base(toValue, duration)
        {
        }

        public BounceAnimation(double toValue, Duration duration, FillBehavior fillBehavior)
            : base(toValue, duration, fillBehavior)
        {
        }

        public BounceAnimation(double fromValue, double toValue, Duration duration)
            : base(fromValue, toValue, duration)
        {
        }

        public BounceAnimation(double fromValue, double toValue, Duration duration, FillBehavior fillBehavior)
            : base(fromValue, toValue, duration, fillBehavior)
        {
        }

        protected override Freezable CreateInstanceCore()
        {
            return new BounceAnimation();
        }

        protected override double GetCurrentValueCore(double defaultOriginValue, double defaultDestinationValue, AnimationClock animationClock)
        {
            double i = (double)animationClock.CurrentProgress * 1.1226;
            double b;

            if (i < 0.3535)
            {
                b = bounce(i);
            }
            else if (i < 0.7408)
            {
                b = bounce(i - 0.54719) + 0.7;
            }
            else if (i < 0.9644)
            {
                b = bounce(i - 0.8526) + 0.9;
            }
            else
            {
                b = bounce(i - 1.0435) + 0.95;
            }

            return (double)From + ((double)To - (double)From) * b;
        }

        protected double bounce(double t)
        {
            return t * t * 8.0;
        }
    }
}
