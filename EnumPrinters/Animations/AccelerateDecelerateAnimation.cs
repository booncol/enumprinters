﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace WpfCustomAnimation
{
    class AccelerateDecelerateAnimation : DoubleAnimation
    {
        public AccelerateDecelerateAnimation()
            : base()
        {
        }

        public AccelerateDecelerateAnimation(double toValue, Duration duration)
            : base(toValue, duration)
        {
        }

        public AccelerateDecelerateAnimation(double toValue, Duration duration, FillBehavior fillBehavior)
            : base(toValue, duration, fillBehavior)
        {
        }

        public AccelerateDecelerateAnimation(double fromValue, double toValue, Duration duration)
            : base(fromValue, toValue, duration)
        {
        }

        public AccelerateDecelerateAnimation(double fromValue, double toValue, Duration duration, FillBehavior fillBehavior)
            : base(fromValue, toValue, duration, fillBehavior)
        {
        }

        protected override Freezable CreateInstanceCore()
        {
            return new AccelerateDecelerateAnimation();
        }

        protected override double GetCurrentValueCore(double defaultOriginValue, double defaultDestinationValue, AnimationClock animationClock)
        {
            double t = Math.Cos(((double)animationClock.CurrentProgress + 1.0) * Math.PI) * 0.5 + 0.5;
            return (double)From + ((double)To - (double)From) * t;
        }
    }
}
