﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace WpfCustomAnimation
{
    public class Animations
    {
        public static void Fade(UIElement element,
                                double fromAlpha,
                                double toAlpha,
                                double duration,
                                double delay,
                                DoubleAnimation animation,
                                EventHandler finishedEvent = null)
        {
            DoubleAnimation fadeAnimation = GetAnimation(animation, fromAlpha, toAlpha, duration, delay);
            if (finishedEvent != null)
            {
                fadeAnimation.Completed += finishedEvent;
            }

            element.Opacity = fromAlpha;
            element.BeginAnimation(UIElement.OpacityProperty, fadeAnimation);
        }

        public static void Scale(UIElement element,
                                 double fromScaleX,
                                 double fromScaleY,
                                 double toScaleX,
                                 double toScaleY,
                                 double duration,
                                 double delay,
                                 DoubleAnimation animation,
                                 EventHandler finishedEvent = null)
        {
            DoubleAnimation scaleXAnimation = GetAnimation(animation, fromScaleX, toScaleX, duration, delay);
            DoubleAnimation scaleYAnimation = GetAnimation(animation, fromScaleY, toScaleY, duration, delay);
            if (finishedEvent != null)
            {
                scaleYAnimation.Completed += finishedEvent;
            }

            ScaleTransform transform = new ScaleTransform(fromScaleX, fromScaleY);
            element.RenderTransform = transform;
            element.RenderTransformOrigin = new Point(0.5, 0.5);

            transform.BeginAnimation(ScaleTransform.ScaleXProperty, scaleXAnimation);
            transform.BeginAnimation(ScaleTransform.ScaleYProperty, scaleYAnimation);
        }

        public static void Translate(UIElement element,
                                     double fromX,
                                     double fromY,
                                     double toX,
                                     double toY,
                                     double duration,
                                     double delay,
                                     DoubleAnimation animation,
                                     EventHandler finishedEvent = null)
        {
            DoubleAnimation animationX = GetAnimation(animation, fromX, toX, duration, delay);
            DoubleAnimation animationY = GetAnimation(animation, fromY, toY, duration, delay);
            if (finishedEvent != null)
            {
                animationY.Completed += finishedEvent;
            }

            TranslateTransform transform = new TranslateTransform(fromX, fromY);
            element.RenderTransform = transform;

            transform.BeginAnimation(TranslateTransform.XProperty, animationX);
            transform.BeginAnimation(TranslateTransform.YProperty, animationY);
        }

        public static double GetOpacity(UIElement element)
        {
            return element.Opacity;
        }

        public static DoubleAnimation GetAnimation(DoubleAnimation animation,
                                                   double from,
                                                   double to,
                                                   double duration,
                                                   double delay)
        {
            DoubleAnimation result = animation.Clone();
            result.From = from;
            result.To = to;
            result.Duration = new Duration(TimeSpan.FromMilliseconds(duration));
            result.BeginTime = TimeSpan.FromMilliseconds(delay);

            return result;
        }
    }
}
