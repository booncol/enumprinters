﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EnumPrinters
{
    public class NinePatchDecorator : Decorator
    {
        #region ImageSource

        public BitmapSource ImageSource
        {
            get { return (BitmapSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ImageSourceProperty = DependencyProperty.Register("ImageSource",
            typeof(ImageSource), typeof(NinePatchDecorator),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion

        #region ImageOpacity

        public double ImageOpacity
        {
            get { return (double)GetValue(ImageOpacityProperty); }
            set { SetValue(ImageOpacityProperty, value); }
        }

        public static readonly DependencyProperty ImageOpacityProperty =
            DependencyProperty.Register("ImageOpacity", typeof(double), typeof(NinePatchDecorator),
                new FrameworkPropertyMetadata(1D, FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion

        #region ImageMargin

        public Thickness ImageMargin
        {
            get { return (Thickness)GetValue(ImageMarginProperty); }
            set { SetValue(ImageMarginProperty, value); }
        }

        public static readonly DependencyProperty ImageMarginProperty =
            DependencyProperty.Register("ImageMargin", typeof(Thickness), typeof(NinePatchDecorator),
                new FrameworkPropertyMetadata(new Thickness(), FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion

        #region FillCenter

        public bool FillCenter
        {
            get { return (bool)GetValue(FillCenterProperty); }
            set { SetValue(FillCenterProperty, value); }
        }

        public static readonly DependencyProperty FillCenterProperty =
            DependencyProperty.Register("FillCenter", typeof(bool), typeof(NinePatchDecorator),
                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

        #endregion

        #region Properties

        private bool IsNineGrid
        {
            get { return !ImageMargin.Equals(new Thickness()); }
        }

        #endregion

        protected override void OnRender(DrawingContext drawingContext)
        {
            Thickness margin = Clamp(ImageMargin, new Size(ImageSource.PixelWidth, ImageSource.Height), RenderSize);

            int w1 = (int)ImageMargin.Left;
            int h1 = (int)ImageMargin.Top;
            int w3 = (int)ImageMargin.Right;
            int h3 = (int)ImageMargin.Bottom;
            int w2 = ImageSource.PixelWidth - w1 - w3;
            int h2 = ImageSource.PixelHeight - h1 - h3;

            drawingContext.PushOpacity(ImageOpacity);
            drawingContext.DrawImage(CreateCroppedBitmap(0, 0, w1, h1), new Rect(0, 0, w1, h1));
            drawingContext.DrawImage(CreateCroppedBitmap(w1, 0, w2, h1), new Rect(w1, 0, ActualWidth - w1 - w3, h1));
            drawingContext.DrawImage(CreateCroppedBitmap(w1 + w2, 0, w3, h1), new Rect(ActualWidth - w3, 0, w3, h1));

            drawingContext.DrawImage(CreateCroppedBitmap(0, h1, w1, h2), new Rect(0, h1, w1, ActualHeight - h1 - h3));
            if (FillCenter)
            {
                drawingContext.DrawImage(CreateCroppedBitmap(w1, h1, w2, h2), new Rect(w1, h1, ActualWidth - w1 - w3, ActualHeight - h1 - h3));

            }
            drawingContext.DrawImage(CreateCroppedBitmap(w1 + w2, h1, w3, h2), new Rect(ActualWidth - w3, h1, w3, ActualHeight - h1 - h3));

            drawingContext.DrawImage(CreateCroppedBitmap(0, h1 + h2, w1, h3), new Rect(0, ActualHeight - h3, w1, h3));
            drawingContext.DrawImage(CreateCroppedBitmap(w1, h1 + h2, w2, h3), new Rect(w1, ActualHeight - h3, ActualWidth - w1 - w3, h3));
            drawingContext.DrawImage(CreateCroppedBitmap(w1 + w2, h1 + h2, w3, h3), new Rect(ActualWidth - w3, ActualHeight - h3, w3, h3));
            drawingContext.Pop();
        }

        private static Thickness Clamp(Thickness margin, Size firstMax, Size secondMax)
        {
            double left = Clamp(margin.Left, firstMax.Width, secondMax.Width);
            double top = Clamp(margin.Top, firstMax.Height, secondMax.Height);
            double right = Clamp(margin.Right, firstMax.Width - left, secondMax.Width - left);
            double bottom = Clamp(margin.Bottom, firstMax.Height - top, secondMax.Height - top);

            return new Thickness(left, top, right, bottom);
        }

        private static double Clamp(double value, double firstMax, double secondMax)
        {
            return Math.Max(0, Math.Min(Math.Min(value, firstMax), secondMax));
        }

        private CroppedBitmap CreateCroppedBitmap(int x, int y, int width, int height)
        {
            return new CroppedBitmap(ImageSource, new Int32Rect(x, y, width, height));
        }
    }
}
