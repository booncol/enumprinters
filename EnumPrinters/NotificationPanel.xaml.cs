﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using WpfCustomAnimation;

namespace EnumPrinters
{
    /// <summary>
    /// Interaction logic for NotificationPanel.xaml
    /// </summary>
    public partial class NotificationPanel : UserControl
    {
        public enum Duration
        {
            VeryShort = 1000,
            Short = 2000,
            Medium = 3000,
            Long = 4000,
            VeryLong = 6000
        }

        public enum Priority
        {
            Lowest,
            Highest,
        }

        private class Message
        {
            private readonly string _text;
            private readonly Duration _duration;

            public Message(string text, Duration duration)
            {
                this._text = text;
                this._duration = duration;
            }

            public string Text
            {
                get
                {
                    return _text;
                }
            }

            public Duration Duration
            {
                get
                {
                    return _duration;
                }
            }
        }

        private LinkedList<Message> _queue = new LinkedList<Message>();

        public NotificationPanel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Animations.Translate(this, 0, this.ActualHeight, 0, this.ActualHeight, 0, 0, new DecelerateAnimation());
        }

        public void PushMessage(string text, Priority priority = Priority.Lowest, Duration duration = Duration.Short)
        {
            lock (_queue)
            {
                if (priority == Priority.Lowest)
                {
                    _queue.AddLast(new Message(text, duration));
                }
                else if (priority == Priority.Highest)
                {
                    _queue.AddFirst(new Message(text, duration));
                }

                if (_queue.Count == 1)
                {
                    ShowFirstMessage();
                }
            }
        }

        private void ShowFirstMessage()
        {
            Message message = _queue.First();
            MessageText.Text = message.Text;

            Animations.Translate(this, 0, this.ActualHeight, 0, 0, 350, 0, new DecelerateAnimation(), delegate(object sender1, EventArgs e1)
            {
                Animations.Translate(this, 0, 0, 0, this.ActualHeight, 350, (double)message.Duration, new AccelerateAnimation(), delegate(object sender2, EventArgs e2)
                {
                    lock (_queue)
                    {
                        _queue.Remove(message);
                        if (_queue.Count > 0)
                        {
                            ShowFirstMessage();
                        }
                    }
                });
            });
        }
    }
}
