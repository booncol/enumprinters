﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Printing;
using System.Drawing.Printing;


namespace EnumPrinters
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (String printerName in PrinterSettings.InstalledPrinters)
            {
                PrintersListBox.Items.Add(printerName);
            }
        }

        private void MenuItemCopy_Click(object sender, RoutedEventArgs e)
        {
            if (PrintersListBox.SelectedIndex < 0)
            {
                return;
            }

            String name = (String)PrintersListBox.Items.GetItemAt(PrintersListBox.SelectedIndex);
            Clipboard.SetText(name);

            Notification.PushMessage("Copied to clipboard");
        }
    }
}
